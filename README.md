# Simple Commandline arguments parser for common lisp

## How to use it:

you must create a list with the functions you want to be executed when x argument is provided

```lisp
(defparameter my-args '(((format t "Hello~%") "--hello")
			   ((format t "~a has been provided.~%"*s-arg:*optarg*) "--use" "-s")))
```

`*optarg*` contains the argumen if the functions needs one.

`do-args` is the function where you provide your list

`cmd-line-args` is the function who returns all the command line arguments provided

## Full example:
```lisp
(defparameter *sigma* nil)

(defparameter my-args '(((print s-arg:*optargs*) "--si" "-s")
                        ((format t "This just works~%") "--print")
			((setq *sigma* ((lambda (x) (+ (* x x) x))
					10)) "-s")))

(defun run-test ()
  (s-arg:do-args my-args :term-arguments (s-arg:cmd-line-args))
  (format t "~a~%~%" (s-arg:cmd-line-args))
  (if(not(null *sigma*))
  (format t "*sigma* value is -> ~a~%" *sigma*)))
```


