(load "~/quicklisp/setup.lisp")
(ql:quickload "asdf")
(asdf:load-system 'simple-args)

(defparameter *sigma* nil)

(defparameter my-args '(((print s-arg:*optargs*) "--si" "-s")
                        ((format t "This just works~%") "--print")
                        ((setq *sigma* ((lambda (x) (+ (* x x) x))
                                        10)) "-s")))

(defun run-test ()
  (s-arg:do-args my-args :term-arguments (s-arg:cmd-line-args))
  (format t "~a~%~%" (s-arg:cmd-line-args))
  (if(not(null *sigma*))
  (format t "*sigma* value is -> ~a~%" *sigma*)))


(run-test)
