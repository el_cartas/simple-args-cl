(asdf:defsystem :simple-args
  :version "0.1"
  :Description "Simple command line arguments"
  :author "El cartas"
  :licence "BSD-0"
  :long-description
  "Simple Args is a little library to parse command line arguments arguments"
  :components  ((:static-file "licence")
                 (:file "simple-args")))

 
