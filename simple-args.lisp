(defpackage :simple-args
  (:nicknames :s-arg)
  (:use #:common-lisp)
  (:export
    #:do-args
    #:cmd-line-args 
    :*optargs*))

(in-package #:simple-args)
  
(defparameter *optargs* nil)

(defun cmd-line-args ()
  #+abcl ext:*command-line-argument-list*
  #+allegro (sys:command-line-arguments)
  #+clisp (cons *load-truename* ext:*args*)
  #+clozure ccl:*command-line-argument-list*
  #+cmu extensions:*command-line-words*
  #+ecl (ext:command-args)
  #+gcl si:*command-args*
  #+lispworks system:*line-arguments-list*
  #+sbcl sb-ext:posix-argv*)
                                
;;example
#|(defparameter my-args-4 '(((setf *testeo* "it works") "--execute" "-e")
                          ((print *optargs*) "--help" "-h")
                          ((delete 1) "--algol")
                          ((format t "Here testing a lambda: ~a" ((lambda (x) (* x x)) 10)) "--quiet")))|#

(defun do-args (argument-list &key (n 0) (term-arguments (cmd-line-args)))
  (if(null argument-list)
    nil
    (do-all (car (nth n argument-list)) (cdr (nth n argument-list)) term-arguments))
  (if(not(>= n (length (cdr argument-list))))
    (do-args argument-list :n (1+ n) :term-arguments term-arguments)
    nil))

(defun do-all (funct-list argl term-arguments)
  (let ((arguments (if(eql 1 (length argl))
                     (append argl argl)
                     argl)))
    (let ((arguments-pos (argp (cdr term-arguments) arguments 0)))
      (if(null arguments-pos)
        nil
        (if (member '*optargs* funct-list :test #'equalp)
            (progn (setq *optargs* (nth (+ arguments-pos 2) term-arguments))
                   (eval funct-list))
            (eval funct-list))))))

(defun argp (l str n)
  (cond ((null l) nil)
        ((or(string-equal (remove #\space (car l)) (car str))
           (string-equal (remove #\space (car l)) (cadr str))) n)
        (t (argp (cdr l) str (1+ n)))))
